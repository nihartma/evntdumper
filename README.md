# EVNTDumper

This will dump selected events from EVNT files and a flat TTree containing the MC record.
The list of events has to be stored in a TTree (with branches `event_number/l`, `run_number/I`) and placed in `EventPicker/data/event_list.root`.
The joboptions are in [ParticleListDumper/share/ParticleListDumperAlgJobOptions.py](ParticleListDumper/share/ParticleListDumperAlgJobOptions.py)

# Setup
In the code directory:
```
mkdir build
cd build
acmSetup AthAnalysis,21.2.63
acm compile
```

# Run locally
```
athena ParticleListDumperAlgJobOptions.py [--filesInput input_files]
```

# Run on the grid
```
[lsetup panda]
pathena ParticleListDumperAlgJobOptions.py [--inDS=input_dataset --outDS=output_dataset]
```
