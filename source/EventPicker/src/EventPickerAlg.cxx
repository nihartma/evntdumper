#include <fstream>
#include <unordered_set>
#include <sstream>

// EventPicker includes
#include "EventPickerAlg.h"

//#include "xAODEventInfo/EventInfo.h"

#include "EventInfo/EventIncident.h"
#include "EventInfo/EventInfo.h"
#include "EventInfo/EventID.h"
#include "EventInfo/EventType.h"

#include "PathResolver/PathResolver.h"

EventPickerAlg::EventPickerAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration

}


EventPickerAlg::~EventPickerAlg() {}

unsigned long EventPickerAlg::get_hash(long long run_number, int event_number) {
  std::stringstream hash_ss;
  hash_ss << std::internal << std::setfill('0') << std::setw(20) << run_number;
  hash_ss << std::internal << std::setfill('0') << std::setw(20) << event_number;
  return std::hash<std::string>{}(hash_ss.str());
}

StatusCode EventPickerAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  //
  //This is called once, before the start of the event loop
  //Retrieves of tools you have configured in the joboptions go here
  //

  //HERE IS AN EXAMPLE
  //We will create a histogram and a ttree and register them to the histsvc
  //Remember to configure the histsvc stream in the joboptions
  //
  //m_myHist = new TH1D("myHist","myHist",100,0,100);
  //CHECK( histSvc()->regHist("/MYSTREAM/myHist", m_myHist) ); //registers histogram to output stream
  //m_myTree = new TTree("myTree","myTree");
  //CHECK( histSvc()->regTree("/MYSTREAM/SubDirectory/myTree", m_myTree) ); //registers tree to output stream inside a sub-directory

  TFile* rootfile = TFile::Open(PathResolverFindCalibFile("EventPicker/event_list.root").c_str());
  TTree* tree = (TTree*)rootfile->Get("event_list");
  ULong64_t event_number;
  Int_t run_number;
  tree->SetBranchAddress("event_number", &event_number);
  tree->SetBranchAddress("run_number", &run_number);
  for (int i=0; i<tree->GetEntriesFast(); i++) {
    tree->GetEntry(i);
    unsigned long hash = get_hash(run_number, event_number);
    m_event_index.insert(hash);
  }


  return StatusCode::SUCCESS;
}

StatusCode EventPickerAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //


  return StatusCode::SUCCESS;
}

StatusCode EventPickerAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed


  const EventInfo* pEvent;
  CHECK( evtStore()->retrieve(pEvent) );

  long long event_number = pEvent->event_ID()->event_number();
  int run_number = pEvent->event_ID()->run_number();
  if (m_event_index.find(get_hash(run_number, event_number)) != m_event_index.end()) {
    ATH_MSG_INFO ("Event " << run_number << " " << event_number << " passed");
    setFilterPassed(true);
  }


  return StatusCode::SUCCESS;
}

StatusCode EventPickerAlg::beginInputFile() { 
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  // const xAOD::CutBookkeeperContainer* bks = 0;
  // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );



  return StatusCode::SUCCESS;
}

