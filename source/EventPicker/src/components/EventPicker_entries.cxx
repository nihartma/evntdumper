
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../EventPickerAlg.h"

DECLARE_ALGORITHM_FACTORY( EventPickerAlg )

DECLARE_FACTORY_ENTRIES( EventPicker ) 
{
  DECLARE_ALGORITHM( EventPickerAlg );
}
