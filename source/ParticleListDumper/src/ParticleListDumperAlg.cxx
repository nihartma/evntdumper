// ParticleListDumper includes
#include "ParticleListDumperAlg.h"

//#include "xAODEventInfo/EventInfo.h"


#include "EventInfo/EventIncident.h"
#include "EventInfo/EventInfo.h"
#include "EventInfo/EventID.h"
#include "EventInfo/EventType.h"

#include "GeneratorObjects/McEventCollection.h"

ParticleListDumperAlg::ParticleListDumperAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration

}


ParticleListDumperAlg::~ParticleListDumperAlg() {}


StatusCode ParticleListDumperAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  //
  //This is called once, before the start of the event loop
  //Retrieves of tools you have configured in the joboptions go here
  //

  //HERE IS AN EXAMPLE
  //We will create a histogram and a ttree and register them to the histsvc
  //Remember to configure the histsvc stream in the joboptions
  //
  //m_myHist = new TH1D("myHist","myHist",100,0,100);
  //CHECK( histSvc()->regHist("/MYSTREAM/myHist", m_myHist) ); //registers histogram to output stream

  m_tree = new TTree("mcParticles","mcParticles");
  CHECK( histSvc()->regTree("/MYSTREAM/mcParticles", m_tree) );

  m_tree->Branch("dsid", &m_dsid);
  m_tree->Branch("event_number", &m_event_number);
  m_tree->Branch("barcode", &m_barcode);
  m_tree->Branch("pdg_id", &m_pdg_id);
  m_tree->Branch("production_vertex", &m_production_vertex);
  m_tree->Branch("end_vertex", &m_end_vertex);
  m_tree->Branch("status", &m_status);
  m_tree->Branch("px", &m_px);
  m_tree->Branch("py", &m_py);
  m_tree->Branch("pz", &m_pz);
  m_tree->Branch("e", &m_e);

  return StatusCode::SUCCESS;
}

StatusCode ParticleListDumperAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //


  return StatusCode::SUCCESS;
}

StatusCode ParticleListDumperAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed

  //
  //Your main analysis code goes here
  //If you will use this algorithm to perform event skimming, you
  //should ensure the setFilterPassed method is called
  //If never called, the algorithm is assumed to have 'passed' by default
  //


  //HERE IS AN EXAMPLE
  //const xAOD::EventInfo* ei = 0;
  const EventInfo* pEvent;
  //CHECK( evtStore()->retrieve( ei , "EventInfo" ) );
  CHECK( evtStore()->retrieve(pEvent) );
  //ATH_MSG_INFO("eventNumber=" << ei->eventNumber() );
  ATH_MSG_INFO("eventNumber=" << pEvent->event_ID()->event_number());
  ATH_MSG_INFO("runNumber=" << pEvent->event_ID()->run_number());

  m_event_number = pEvent->event_ID()->event_number();
  m_dsid = pEvent->event_ID()->run_number();

  //m_myHist->Fill( ei->averageInteractionsPerCrossing() ); //fill mu into histogram


  // Retrieve the HepMC truth:
  const McEventCollection* mcColl = 0;
  CHECK( evtStore()->retrieve( mcColl, "GEN_EVENT" ) );

  //std::cout << mcColl->size() << std::endl;

  const HepMC::GenEvent* genEvt = (*mcColl)[0];

  // std::cout << genEvt->particles_size() << " particles in this event" << std::endl;

  // find top 100 pt
  // std::vector<std::pair<int, float>> particle_pts;
  // std::set<int> top_100_barcodes;
  // float px, py;
  // for (auto ip = genEvt->particles_begin(); ip != genEvt->particles_end(); ++ip) {
  //   const HepMC::GenParticle* particle = *ip;
  //   // std::cout << particle->barcode() << std::endl;
  //   px = particle->momentum().px();
  //   py = particle->momentum().py();
  //   particle_pts.push_back(std::make_pair(particle->barcode(), px*px+py*py));
  // }
  // std::sort(particle_pts.begin(), particle_pts.end(),
  //           [](const std::pair<int,float>& a,
  //              const std::pair<int,float>& b) -> bool
  //           {
  //             return a.second > b.second;
  //           });
  // int i = 0;
  // for (auto barcode_pt : particle_pts) {
  //   top_100_barcodes.insert(barcode_pt.first);
  //   i++;
  //   if (i >= 100) {
  //     break;
  //   }
  // }


  for (auto ip = genEvt->particles_begin(); ip != genEvt->particles_end(); ++ip) {
    const HepMC::GenParticle* particle = *ip;
    // print only top 100 in original order
    // if (top_100_barcodes.find(particle->barcode()) == top_100_barcodes.end()) {
    //   continue;
    // }
    //std::cout << particle->barcode() << std::endl;

    m_barcode = particle->barcode();
    m_pdg_id = particle->pdg_id();
    m_px = particle->momentum().px();
    m_py = particle->momentum().py();
    m_pz = particle->momentum().pz();
    m_e = particle->momentum().e();
    m_status = particle->status();

    auto production_vertex = particle->production_vertex();
    auto end_vertex = particle->end_vertex();

    m_production_vertex = production_vertex ? production_vertex->barcode() : 0;
    m_end_vertex = end_vertex ? end_vertex->barcode() : 0;

    m_tree->Fill();

    // std::cout << " ";
    // std::cout.width(9);
    // std::cout << particle->barcode();
    // std::cout.width(9);
    // std::cout << particle->pdg_id() << " ";
    // std::cout.width(9);
    // std::cout.precision(2);
    // std::cout.setf(std::ios::scientific, std::ios::floatfield);
    // std::cout.setf(std::ios_base::showpos);
    // std::cout << particle->momentum().px() << ",";
    // std::cout.width(9);
    // std::cout.precision(2);
    // std::cout << particle->momentum().py() << ",";
    // std::cout.width(9);
    // std::cout.precision(2);
    // std::cout << particle->momentum().pz() << ",";
    // std::cout.width(9);
    // std::cout.precision(2);
    // std::cout << particle->momentum().e() << " ";
    // std::cout.setf(std::ios::fmtflags(0), std::ios::floatfield);
    // std::cout.unsetf(std::ios_base::showpos);
    // if ( particle->has_decayed() ) {
    //   if ( particle->end_vertex()->barcode()!=0 ) {
    //     std::cout.width(3);
    //     std::cout << particle->status() << " ";
    //     std::cout.width(9);
    //     std::cout << particle->end_vertex()->barcode();
    //   }
    // } else {
    //   std::cout.width(3);
    //   std::cout << particle->status();
    //   std::cout.width(9);
    //   auto prod_vtx = particle->production_vertex();
    //   if (prod_vtx) {
    //     std::cout << prod_vtx->barcode();
    //   } else {
    //     std::cout << 0;
    //   }

      // std::cout << particle->production_vertex()->barcode();
    // }
    // std::cout << std::endl;

  }


  setFilterPassed(true); //if got here, assume that means algorithm passed
  return StatusCode::SUCCESS;
}

StatusCode ParticleListDumperAlg::beginInputFile() { 
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  // const xAOD::CutBookkeeperContainer* bks = 0;
  // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );



  return StatusCode::SUCCESS;
}


