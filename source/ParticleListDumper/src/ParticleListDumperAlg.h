#ifndef PARTICLELISTDUMPER_PARTICLELISTDUMPERALG_H
#define PARTICLELISTDUMPER_PARTICLELISTDUMPERALG_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "xAODEventInfo/EventInfo.h"

//Example ROOT Includes
//#include "TTree.h"
//#include "TH1D.h"



class ParticleListDumperAlg: public ::AthAnalysisAlgorithm { 
 public: 
  ParticleListDumperAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~ParticleListDumperAlg(); 

  ///uncomment and implement methods as required

                                        //IS EXECUTED:
  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
  //virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
  virtual StatusCode  execute();        //per event
  //virtual StatusCode  endInputFile();   //end of each input file
  //virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
  virtual StatusCode  finalize();       //once, after all events processed
  

  ///Other useful methods provided by base class are:
  ///evtStore()        : ServiceHandle to main event data storegate
  ///inputMetaStore()  : ServiceHandle to input metadata storegate
  ///outputMetaStore() : ServiceHandle to output metadata storegate
  ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
  ///currentFile()     : TFile* to the currently open input file
  ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp


 private: 

   //Example algorithm property, see constructor for declaration:
   //int m_nProperty = 0;

   //Example histogram, see initialize method for registration to output histSvc
   //TH1D* m_myHist = 0;
   TTree* m_tree = 0;

   int m_dsid;
   Long64_t m_event_number;

   int m_barcode, m_pdg_id, m_production_vertex, m_end_vertex, m_status;
   float m_px, m_py, m_pz, m_e;

};

#endif //> !PARTICLELISTDUMPER_PARTICLELISTDUMPERALG_H
