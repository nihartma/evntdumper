
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../ParticleListDumperAlg.h"

DECLARE_ALGORITHM_FACTORY( ParticleListDumperAlg )

DECLARE_FACTORY_ENTRIES( ParticleListDumper ) 
{
  DECLARE_ALGORITHM( ParticleListDumperAlg );
}
